using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlaryerController : MonoBehaviour
{
    Vector2 _movementInput;
    private Animation ani;
    private bool isFacingRight = true;
    private SpriteRenderer sprite;
    public Animator animator;

    //Enum
    private enum MovermentState { idle, run, attack, dead }

    //
    [Header("Speed Player")]
    [ContextMenuItem("Reset", "resetTheValue")]

    [Tooltip("Option speed form 1 -> 10")]
    [Range(1, 10)]
    public float speed;

    private void resetTheValue()
    {
        speed = 5f;
    }

    private Rigidbody2D rb;

    // Start is called before bthe first frame update
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        ani = GetComponent<Animation>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        ProcessInputs();
        Animation();
    }
    void FixedUpdate()
    {

        Move();

    }
    void ProcessInputs()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        _movementInput = new Vector2(moveX, moveY).normalized;
    }
    void Move()
    {
        rb.velocity = new Vector2(_movementInput.x * speed, _movementInput.y * speed);
    }
    void Animation()
    {
        animator.SetFloat("AniMoveX", _movementInput.x);
        animator.SetFloat("AniMoveY", _movementInput.y);
        animator.SetFloat("AnimMoveMagnitude", _movementInput.magnitude);
    }
}
