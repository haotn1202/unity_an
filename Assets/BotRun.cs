using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotRun : MonoBehaviour
{
    public GameObject Player;

    public float speed = 1f;

    private float distance;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        distance = Vector2.Distance(Player.transform.position, transform.position);

        Vector2 direction = Player.transform.position - transform.position;

        transform.position = Vector2.MoveTowards(this.transform.position, Player.transform.position, speed * Time.deltaTime);
    }
}
